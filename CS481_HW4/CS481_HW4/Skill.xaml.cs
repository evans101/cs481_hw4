﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_HW4
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Skill : ViewCell
    {

         public static readonly BindableProperty skillName = BindableProperty.Create("Name", typeof(string), typeof(Skill), "");

        public string Name
        {
            get { return (string)GetValue(skillName); }
            set { SetValue(skillName, value); }
        }

        public static readonly BindableProperty skillLevel = BindableProperty.Create("Level", typeof(int), typeof(Skill), "");

        public int Level
        {
            get { return (int)GetValue(skillLevel); }
            set { SetValue(skillLevel, value); }
        }

        public static readonly BindableProperty skillXP = BindableProperty.Create("XP", typeof(int), typeof(Skill), "");

        public int XP
        {
            get { return (int)GetValue(skillXP); }
            set { SetValue(skillXP, value); }
        }

        public static readonly BindableProperty skillXPtoLevel = BindableProperty.Create("XPtoLevel", typeof(int), typeof(Skill), "");

        public int XPtoLevel
        {
            get { return (int)GetValue(skillXPtoLevel); }
            set { SetValue(skillXPtoLevel, value); }
        }

        public static readonly BindableProperty skillImageFile = BindableProperty.Create("skillImageFile", typeof(string), typeof(Skill), "");

        public string ImageFile
        {
            get { return (string)GetValue(skillImageFile); }
            set { SetValue(skillImageFile, value); }
        }

        private int CalculateXPtoNextLevel(int currentLevel)
        {
            switch(currentLevel)
            {
                case 1:
                    return 83;
                case 2:
                    return 174;
                case 3:
                    return 276;
                case 4:
                    return 388;
                case 5:
                    return 512;
                case 6:
                    return 650;
                case 7:
                    return 801;
                case 8:
                    return 969;
                case 9:
                    return 1154;
                case 10:
                    return 1000000;
                case 99:
                    return 0;
                default:
                    return -1;

            }
        }

        private int CalculateLevel(int currentXP)
        {
            if (currentXP < 83)
                return 1;
            else if (currentXP < 174)
                return 2;
            else if (currentXP < 276)
                return 3;
            else if (currentXP < 388)
                return 4;
            else if (currentXP < 512)
                return 5;
            else if (currentXP < 650)
                return 6;
            else if (currentXP < 801)
                return 7;
            else if (currentXP < 969)
                return 8;
            else if (currentXP < 1154)
                return 9;
            else if (currentXP < 1000000)
                return 10;
            else if (currentXP >= 1000000)
                return 99;
            else
                return -1;
        }

        public void UpdateSkillData(int xpGained)
        {
            XP += xpGained;
            Level = CalculateLevel(XP);
            XPtoLevel = CalculateXPtoNextLevel(Level);
        }
    }
}