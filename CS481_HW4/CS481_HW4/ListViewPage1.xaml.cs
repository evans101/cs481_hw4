﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_HW4
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ListViewPage1 : ContentPage
    {
        public ObservableCollection<Skill> Skills { get; set; }

        public ListViewPage1()
        {
            InitializeComponent();
            Skills = new ObservableCollection<Skill>();
            CreateSkills();
        }

        async void Handle_TrainClicked(object sender, EventArgs e)
        {
            Skill skillToTrain = sender as Skill;
            await DisplayAlert("Training Skill", $"Now training {skillToTrain.Name}.", "OK");
            await Navigation.PushAsync(new SkillPage(skillToTrain));
        }

         void CreateSkills()
        {
            Skill newSkill = new Skill
            {
            Name = "Mining",
            Level = 1,
            XP = 0,
            XPtoLevel = 83,
            ImageFile = "mining.jpg"
            };

            Skills.Add(newSkill);
            newSkill.Name = "Fishing";
            newSkill.ImageFile = "fishing.jpg";
            Skills.Add(newSkill);
            newSkill.Name = "Cooking";
            newSkill.ImageFile = "cooking.jpg";
            Skills.Add(newSkill);
            newSkill.Name = "Smithing";
            newSkill.ImageFile = "smithing.jpg";
            Skills.Add(newSkill);
        }

        //https://xamarinhelp.com/pull-to-refresh-listview/
        async void Handle_SkillsRefreshing(object sender, EventArgs e)
        {
            SkillsListView.IsRefreshing = true;
            Skills.Clear();
            CreateSkills();
            await DisplayAlert("Alert", "Skills Refreshed.", "OK");
            SkillsListView.IsRefreshing = false;
        }
    }
}
